from django.urls import path
from .views import create_ingredient, edit_ingredient, delete_ingredient

urlpatterns = [
    path("create/", create_ingredient, name="create_ingredient"),
    path('<int:id>/edit/', edit_ingredient, name="edit_ingredient"),
    path("<int:id>/delete/", delete_ingredient, name="delete_ingredient"),
]