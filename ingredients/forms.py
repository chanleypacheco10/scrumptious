from django.forms import ModelForm
from .models import Ingredient


class CreateIngredientForm(ModelForm):
    class Meta:
        model = Ingredient
        fields = [
            "amount",
            "food_item",
            "recipe"
        ]