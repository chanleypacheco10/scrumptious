from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .forms import CreateIngredientForm
from ingredients.models import Ingredient

@login_required
def create_ingredient(request):
    if request.method == 'POST':
        form = CreateIngredientForm(request.POST)
        if form.is_valid():
            ingredient = form.save(False)
            ingredient.author = request.user
            ingredient.save()
            return redirect('show_recipe', id=ingredient.recipe.id)
    else: 
        form = CreateIngredientForm()
    
    context = {
        'form': form,
    }

    return render(request, 'ingredients/create.html', context)

@login_required
def edit_ingredient(request, id):
    ingredient = get_object_or_404(Ingredient, id=id)
    if request.method == "POST":
        form = CreateIngredientForm(request.POST, instance=ingredient)
        if form.is_valid():
            form.save()
            return redirect('show_recipe', id=ingredient.recipe.id)
    else:
        form = CreateIngredientForm(instance=ingredient)
    
    context = {
        'form': form,
        'ingredient_object': ingredient
    }

    return render(request, 'ingredients/edit.html', context)


@login_required
def delete_ingredient(request, id):
    ingredient = get_object_or_404(Ingredient, id=id)
    if request.method == "POST":
        ingredient.delete()
        return redirect("show_recipe", id=ingredient.recipe.id)
    
    context = {
        "ingredient_object": ingredient,
    }

    return render(request, "ingredients/delete.html", context)