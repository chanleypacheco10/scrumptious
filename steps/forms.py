from django.forms import ModelForm
from .models import RecipeStep

# Define a form for creating RecipeStep instances
class CreateStepForm(ModelForm):
    class Meta:
        model = RecipeStep  
        fields = [
            "instruction",  
            "recipe"  
        ]
