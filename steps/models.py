from django.db import models
from recipes.models import Recipe

# Model for representing individual steps in a recipe
class RecipeStep(models.Model):
    step_number = models.PositiveSmallIntegerField()
    instruction = models.TextField()
    recipe = models.ForeignKey(
        Recipe,  # The related Recipe model
        related_name='steps',  # The reverse relation name for accessing steps from a Recipe instance
        on_delete=models.CASCADE  # On deletion of a Recipe, also delete its asso
    )
