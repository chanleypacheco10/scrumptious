from django.contrib import admin
from .models import RecipeStep

# Register the RecipeStep model with the Django admin site
@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    # Define the fields to be displayed in the admin list view
    list_display = (
        "step_number",  
        "instruction", 
        "id",  
    )
