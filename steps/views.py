from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .forms import CreateStepForm
from .models import RecipeStep


@login_required
def create_step(request):
    if request.method == 'POST':
        # If the form is submitted, validate and process the data
        form = CreateStepForm(request.POST)
        if form.is_valid():
            # Save the form without committing to the database
            step = form.save(commit=False)
            # Set the author (assuming 'author' is a field in your RecipeStep model)
            step.author = request.user
            step.save()

            return redirect('show_recipe', id=step.recipe.id)
    else: 
        form = CreateStepForm()
    
    context = {
        'form': form,
    }

    return render(request, 'steps/create.html', context)


@login_required
def edit_step(request, id):
    # Get the RecipeStep instance with the specified id or return a 404 error
    step = get_object_or_404(RecipeStep, id=id)
    if request.method == "POST":
        form = CreateStepForm(request.POST, instance=step)
        if form.is_valid():
            form.save()
            
            return redirect('show_recipe', id=step.recipe.id)
    else:
        # If the form is not submitted, create a form pre-filled with the existing data
        form = CreateStepForm(instance=step)
    
    context = {
        'form': form,
        'step_object': step
    }

    return render(request, 'steps/edit.html', context)


@login_required
def delete_step(request, id):
    # Get the RecipeStep instance with the specified id or return a 404 error
    step = get_object_or_404(RecipeStep, id=id)
    if request.method == "POST":
        step.delete()
        
        return redirect("show_recipe", id=step.recipe.id)
    
    context = {
        "step_object": step,
    }

   
    return render(request, "steps/delete.html", context)
