from django.urls import path
from .views import create_step, edit_step, delete_step


urlpatterns = [
    path("create/", create_step, name="create_step"),  # URL for creating a new step
    path('<int:id>/edit/', edit_step, name="edit_step"),  # URL for editing an existing step
    path("<int:id>/delete/", delete_step, name="delete_step"),  # URL for deleting an existing step
]
