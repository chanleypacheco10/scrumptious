from django.db import models
from django.conf import settings


#Creating a table named Recipe
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name = 'recipes',
        on_delete = models.CASCADE,
        null = True,
    )

    def __str__(self):
        return self.title
