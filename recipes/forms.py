from django.forms import ModelForm
from recipes.models import Recipe

#define a form for creating Recipe instances
class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description"
        ]