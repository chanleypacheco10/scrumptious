from recipes.models import Recipe
from django.shortcuts import render, get_object_or_404, redirect
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required

# View to edit an existing recipe
@login_required
def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()

            return redirect('recipe_list')
    else:
        form = RecipeForm(instance=recipe)

    context = {
        'recipe_object': recipe,
        'form': form,
    }

    return render(request, 'recipes/edit.html', context)

# View to display details of a specific recipe
def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }

    return render(request, "recipes/detail.html", context)

# View to display a list of all recipes
def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }

    return render(request, "recipes/list.html", context)

# View to display a list of recipes created by the current user
@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }

    return render(request, "recipes/list.html", context)

# View to create a new recipe
@login_required
def create_recipe(request):
    if request.method == 'POST':
        form = RecipeForm(request.POST)
        if form.is_valid():
            recipe = form.save(commit=False)

            recipe.author = request.user
            recipe.save()

            return redirect('recipe_list')
    else: 
        form = RecipeForm()
    
    context = {
        'form': form,
    }

    return render(request, 'recipes/create.html', context)

# View to delete an existing recipe
@login_required
def delete_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)

    if request.user != recipe.author:
        return render(request, "recipes/create.html")

    if request.method == "POST":
        recipe.delete()
        return redirect("recipe_list")

    context = {
        "recipe_object": recipe,
    }

    
    return render(request, "recipes/delete.html", context)
