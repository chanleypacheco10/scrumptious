from django.urls import path
from recipes.views import (
    show_recipe, 
    recipe_list, 
    create_recipe, 
    edit_recipe, 
    my_recipe_list,
    delete_recipe,
)


urlpatterns = [
    path('mine', my_recipe_list, name='my_recipe_list'),  # URL for displaying the current user's recipes
    path('create/', create_recipe, name='create_recipe'),  # URL for creating a new recipe
    path('', recipe_list, name='recipe_list'),  # URL for displaying a list of all recipes
    path('<int:id>/', show_recipe, name='show_recipe'),  # URL for displaying details of a specific recipe
    path('<int:id>/edit/', edit_recipe, name="edit_recipe"),  # URL for editing an existing recipe
    path('<int:id>/delete/', delete_recipe, name="delete_recipe"),  # URL for deleting an existing recipe
]
