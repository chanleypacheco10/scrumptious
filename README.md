# RecipeRise
RecipeRise is a user-friendly recipe management app designed to elevate your culinary experience. Tailored for individual users, the app allows you to effortlessly create, edit, and view recipes based on personalized profiles.

## Team
* Carlos Pacheco 

## How to run the app

1. Run this command within your PowerShell to clone the forked repository onto your local computer:
git clone https://gitlab.com/chanleypacheco10/RecipeRise.git

2. Create a virtual environment and pip install requirements.txt using the following commands:
```
python -m venv .venv
source .venv/bin/activate

pip install -r requirements.txt
```

3. Start server by runnning the folowing command in terminal:
```
python manage.py runserver
```
- After running these commands, verify server is running by navigating to the following address:

- View the project in the browser: http://127.0.0.1:8000

![Img](/Home.jpeg)
