from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.models import User
from accounts.forms import SignUpForm, LoginForm

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            # Extract cleaned data from the form
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']

            # Check if passwords match before creating the user
            if password == password_confirmation:
                author = User.objects.create_user(
                    username=username, 
                    password=password,
                    first_name=first_name,
                    last_name=last_name,
                )
                
                print(author.first_name)
                
                login(request, author)

                return redirect('recipe_list')
            else:
                form.add_error("password", "Passwords do not match!")
    else:
        form = SignUpForm()

    context = {"form": form}
    # Render the 'signup.html' template with the form
    return render(request, "accounts/signup.html", context)


def user_login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            # Extract cleaned data from the form
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            author = authenticate(
                request,
                username=username,
                password=password
            )

            if author is not None:
                login(request, author)
                messages.success(request, 'Login successful.')
                return redirect('recipe_list')
            else:
                messages.error(request, 'Invalid login credentials. Please try again.')
    else:
        form = LoginForm()
    
    context = {'form': form, 'user': request.user}
    return render(request, 'accounts/login.html', context)


def user_logout(request):
    logout(request)
    return redirect('recipe_list')
