from django.urls import path
from accounts.views import signup, user_login, user_logout

urlpatterns = [
    # URL pattern for user logout
    path('logout/', user_logout, name='user_logout'),
    
    # URL pattern for user login
    path("login/", user_login, name="user_login"),
    
    # URL pattern for user signup
    path('signup/', signup, name='signup')
]

