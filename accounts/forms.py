from django import forms

class SignUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    first_name = forms.CharField(max_length=150)
    last_name = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150, 
        widget=forms.PasswordInput,
    )
    # Password confirmation field for sign-up with password input widget
    password_confirmation = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )

    def clean(self):
        """
        Custom clean method to validate that password and password confirmation match.
        """
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_confirmation = cleaned_data.get("password_confirmation")

        # Check if passwords match, raise validation error if not
        if password and password_confirmation and password != password_confirmation:
            raise forms.ValidationError("Passwords do not match")

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(
        max_length=150,
        widget=forms.PasswordInput,
    )




